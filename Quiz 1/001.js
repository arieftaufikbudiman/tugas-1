function balikString(str){
    let reversed = "";    
    for (var i = str.length - 1; i >= 0; i--){        
      reversed += str[i];
    }    
    return reversed;
  }

  function palindrome(str) {
    var newStr= str.replace(/[^0-9a-z]/gi, '').toLowerCase().split("");
    for(var i=0; i < (newStr.length)/2; i++){ 
      if(newStr[i] !== newStr[newStr.length-i-1]){ 
        return false;
      } 
    }
    return true;
  }

  function bandingkan(num1 = 0, num2 =0){
      num1 = parseInt(num1);
      num2 = parseInt(num2);
      if(num1 < 0 || num2 < 0){
          return -1;
      }
      else if(num1 === num2){
          return -1;
      }
      else if(num1 > num2){
          return num1;
      }
      else if(num2 > num1){
          return num2;
      }
  }

console.log("Jawaban Soal 001 A")
console.log(balikString("abcde")) // edcba
console.log(balikString("rusak")) // kasur
console.log(balikString("racecar")) // racecar
console.log(balikString("haji")) // ijah

console.log("\n")
console.log("Jawaban Soal 001 B")
console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta")) // false

console.log("\n")
console.log("Jawaban Soal 001 C")
console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1 
console.log(bandingkan(112, 121));// 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan("15", "18")) // 18