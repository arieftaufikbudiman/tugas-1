function AscendingTen(num = null) {
    if (num === null) {
        return -1
    }
    var array = [];
    for (let index = 0; index < 10; index++) {
        array.push(num)
        num++
    }
    var array2 = []
    array2 = array.join(" ")
    return array2;
}

function DescendingTen(num = null) {
    if (num === null) {
        return -1
    }
    var array = [];
    for (let index = 0; index < 10; index++) {
        array.push(num)
        num--
    }
    var array2 = []
    array2 = array.join(" ")
    return array2;
}


function ConditionalAscDesc(reference = null, check = null) {
    if (reference === null || check === null) {
        return -1
    }
    var array = [];
    if (check % 2 !== 0) {
        var array = [];
        for (let index = 0; index < 10; index++) {
            array.push(reference)
            reference++
        }
        var array2 = []
        array2 = array.join(" ")
        return array2;
    }
    else if (check % 2 === 0) {
        for (let index = 0; index < 10; index++) {
            array.push(reference)
            reference--
        }
        var array2 = []
        array2 = array.join(" ")
        return array2;
    }
    else {
        return -1
    }
}


function ularTangga() {
    var number = 100;
    var arrayTotal = [];
    for (let index = 1; index < 11; index++) {
        if (index % 2 !== 0) {
            var array = [];
            for (let index = 0; index < 10; index++) {
                array.push(number);
                number--
                if(number ===0){
                    break;
                }
            }
            var array2 = []
            array2 = array.join(" ")
            arrayTotal.push(array2);
        }
        else {
            var array = [];
            for (let index = 0; index < 10; index++) {
                array.push(number);
                number--
                if(number ===0){
                    break;
                }
            }
            var array2 = []
            array2 = array.sort().join(" ");
            arrayTotal.push(array2);
        }
    }
    var arrayUlarTanggal = arrayTotal.join("\n");
    return arrayUlarTanggal
}

console.log("\n")
console.log("Jawaban Soal 002 A")
console.log(AscendingTen(11)) // 11 12 13 14 15 16 17 18 19 20
console.log(AscendingTen(21)) // 21 22 23 24 25 26 27 28 29 30
console.log(AscendingTen()) // -1

console.log("\n")
console.log("Jawaban Soal 002 B")
console.log(DescendingTen(100)) // 100 99 98 97 96 95 94 93 92 91
console.log(DescendingTen(10)) // 10 9 8 7 6 5 4 3 2 1
console.log(DescendingTen()) // -1

console.log("\n")
console.log("Jawaban Soal 002 C")
console.log(ConditionalAscDesc(20, 8)) // 20 19 18 17 16 15 14 13 12 11
console.log(ConditionalAscDesc(81, 1)) // 81 82 83 84 85 86 87 88 89 90
console.log(ConditionalAscDesc(31)) // -1
console.log(ConditionalAscDesc()) // -1

console.log("\n")
console.log("Jawaban Soal 002 D")
console.log(ularTangga())