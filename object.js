function arrayToObject(arr) {
    var now = new Date()
    var thisYear = now.getFullYear()
    for (let index = 0; index < arr.length; index++) {
        var obj = {};
        obj.firstName = arr[index][0];
        obj.lastName = arr[index][1];
        obj.gender = arr[index][2];
        if (arr[index][3] === undefined || arr[index][3] > thisYear) {
            obj.age = "Invalid Birth Year";
        } else {
            obj.age = thisYear - arr[index][3];

        }
        console.log(arr[index][0], ' ', arr[index][1], ' : ', obj)
    }
}

var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]]
var people2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]]
console.log('Jawaban No 1')
arrayToObject(people)
arrayToObject(people2)



var Sale = {
    'Sepatu Stacattu': 1500000,
    'Baju Zoro': 500000,
    'Baju H&N': 250000,
    'Sweater Uniklooh': 175000,
    'Casing Handphone': 50000
};

function shoppingTime(memberId = '', money = 0) {
    var obj = {};
    var arr = [];
    if (memberId == '') {
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    }
    else if (money < 50000) {
        return "Mohon maaf, uang tidak cukup"
    }
    else {
        obj.money = money;
        for (var key in Sale) {
            if (money >= Sale[key]) {
                money = money - Sale[key];
                arr.push(key);
            }
        }
        obj.memberId = memberId;
        obj.listPurchased = arr;
        obj.changeMoney = money;
        return obj;
    }
}
console.log('\n');
console.log('Jawaban No 2')
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

function naikAngkot(arrPenumpang) {
    var arr = [];
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    for (let index = 0; index < arrPenumpang.length; index++) {
        var obj = {}
        var naik;
        var turun;
        for (let i = 0; i < rute.length; i++) {
            if (arrPenumpang[index][1] === rute[i]) {
                naik = i
            }
            if (arrPenumpang[index][2] === rute[i]) {
                turun = i
            }
        }
        obj.penumpang = arrPenumpang[index][0];
        obj.NaikDari = arrPenumpang[index][1];
        obj.tujuan = arrPenumpang[index][2];
        obj.bayar = 2000 * (turun - naik)
        arr.push(obj);
    }
    return arr;
}

console.log('\n');
console.log('Jawaban No 3');
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
console.log(naikAngkot([])); //[]