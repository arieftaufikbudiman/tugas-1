class Score {
    constructor(subject, points, email) {
        this.email = email;
        this.subject = subject;
        this.points = points;
       
    }

    set sub(s){
        this.subject = s;
    }

    set poin(p){
        this.points = p;
    }
    
    set ema(e){
        this.email=e;
    }

    Average() {
        if (typeof (this.points) === 'object') {
            let sum = 0;
            this.points.forEach(element => {
                sum += element;
            });
            return sum / this.points.length;
        }
        else {
            return this.points;
        }
    }
}

console.log("Jawaban Soal 1");
var Kelas1 = new Score("shaun", [5, 6, 7], 'arieftaufikbudiman@gmail.com');
var Kelas2 = new Score("shaun", 8, 'arieftaufikbudiman@gmail.com');

console.log(Kelas1.Average())
console.log(Kelas2.Average())

const data = [
    ["email", "quiz-1", "quiz-2", "quiz-3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88],
    ["bondra@mail.com", 70, 75, 78],
    ["regi@mail.com", 91, 89, 93]
  ]



viewScores = (data, subject) => {
    var indexSubject = data[0].indexOf(subject);
    let array = [];
    for (let i = 0; i < data.length; i++) {
       var Kelas = new Score();
        for (let j = 0; j < array.length; j++) {
            Kelas.ema = data[i][0];
            Kelas.poin =  data[i][indexSubject];
            Kelas.sub = subject;
        }
        array.push(Kelas)
    }
    array.shift();
    console.log(array);
}

console.log("Jawaban Soal 2")
viewScores(data, "quiz-1")
viewScores(data, "quiz-2")
viewScores(data, "quiz-3")

recapScores = (data) => {
    let array = [];
    for (let i = 0; i < data.length; i++) {
       var Kelas = new Score();
        Kelas.email = data[i][0];
        Kelas.points = [data[i][1], data[i][2], data[i][3]]
        if(Kelas.Average() > 90 ){
            Kelas.Predikat = "honour"
        }
        else if (Kelas.Average() > 80 ) {
            Kelas.Predikat = "graduate"
        }
        else {
            Kelas.Predikat = "participant"
        }
        Kelas.Rata = Kelas.Average();
        array.push(Kelas)
    }
    array.shift();
    array.forEach((element, index) =>{
       console.log(`${index + 1}. Email :${element.email}`);
       console.log(`Rata-rata: ${element.Rata}`);
       console.log(`Predikat: ${element.Predikat}`);
       console.log('\n')
    })
}


console.log("Jawaban Soal 3")
recapScores(data)