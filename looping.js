// Jawaban Soal 1
console.log('LOOPING PERTAMA')
for (var i = 2; i <= 20; i = i + 2) {
    console.log(i + " - I love coding")
}


for (var i = 20; i > 0; i = i - 2) {
    console.log(i + " I will become  mobile developer");
}

console.log( "\n" +"LOOPING KEDUA" )
// Jawaban Soal 2
for (var i = 1; i <= 20; i++) {
    if (i % 3 === 0) {
        console.log(i + "- I Love Coding")
    }
    else if (i % 2 === 1) {
        console.log(i + "- Santai")
    }
    else if (i % 2 === 0) {
        console.log(i + "- Berkualitas")
    }
}

// Jawaban Soal 3
console.log( "\n" +"LOOPING KETIGA")
for (let index = 0; index < 4; index++) {
    console.log("####")
}

// Jawaban Soal 4
console.log( "\n" +"LOOPING KEEMPAT")
for (let index = 1; index <= 7; index++) {
    console.log("#".repeat(index))
}

// Jawaban Soal 5
console.log( "\n" +"LOOPING KELIMA")
for (var i = 1; i <= 8; i++) {
    if (i % 2 === 1) {
        console.log(" #".repeat(4))
    }
    else if (i % 2 === 0) {
        console.log("# ".repeat(4))
    }
}