// Jawaban Soal 1
var nama = "John"
var peran = ""
if(nama === '' && peran === '')
{
    console.log("Nama harus diisi!")
}
else if(peran ===''){
    console.log("Halo " + nama + ", Pilih peranmu untuk memulai game!")
}
else if(peran ==='Penyihir'){
    console.log("Selamat datang di Dunia Werewolf, " + nama + '\n' + "Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!" )
}
else if(peran==="Guard"){
    console.log("Selamat datang di Dunia Werewolf, " + nama + '\n' + "Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf." )
}
else if(peran === "Werewolf"){
    console.log("Selamat datang di Dunia Werewolf, " + nama + '\n' + "Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!" ) 
}

// Jawaban Soal 2
var hari = 21; 
var bulan = 8; 
var tahun = 1945;
var NamaBulan = ''

switch(bulan){
    case 1  : NamaBulan = "Januari";   break;
    case 2  : NamaBulan = "Februari";   break;
    case 3  : NamaBulan = "Maret";   break;
    case 4  : NamaBulan = "April";  break;
    case 5  : NamaBulan = "Mei";  break;
    case 6  : NamaBulan = "Juni";   break;
    case 7  : NamaBulan = "Juli";  break;
    case 8  : NamaBulan = "Agustus";  break;
    case 9  : NamaBulan = "September";  break;
    case 10 : NamaBulan = "Oktober";  break;
    case 11 : NamaBulan = "November";  break;
    case 12 : NamaBulan = "Desember";  break;
}
console.log("\n");
console.log(hari + " " + NamaBulan + " " + tahun);