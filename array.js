//Jawaban Soal 1
var arrayNumber = [];
function range(startNum = null, finishNum = null) {
    arrayNumber = [];
    let number = finishNum - startNum
    if (startNum === null || finishNum === null) {
        return -1;
    }
    else if (startNum < finishNum) {
        for (let index = 0; index <= number; index++) {
            arrayNumber.push(startNum);
            startNum++;
        }
        return arrayNumber;
    }
    else if (startNum > finishNum) {
        for (let index = 0; index >= number; index--) {

            arrayNumber.push(startNum)
            startNum--;
        }
        return arrayNumber;
    }
}
console.log("Jawaban Soal 1");
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11, 18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

// Jawaban Soal 2
function rangeWithStep(startNum = null, finishNum = null, step = null) {
    arrayNumber = [];
    let number = (finishNum - startNum) / step
    if (startNum === null || finishNum === null) {
        return -1;
    }
    else if (startNum < finishNum) {
        for (let index = 0; index <= number; index++) {
            arrayNumber.push(startNum);
            startNum = startNum + step;
        }
        return arrayNumber;
    }
    else if (startNum > finishNum) {
        for (let index = 0; index >= number; index--) {

            arrayNumber.push(startNum)
            startNum = startNum - step;
        }
        return arrayNumber;
    }

}
console.log("\n");
console.log("Jawaban Soal 2");
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 


// Jawaban Soal 3
function sum(startNum = null, finishNum = null, step = 1) {
    arrayNumber = [];
    let number = (finishNum - startNum) / step
    if (step === null && startNum === null && finishNum === null) {
        return 0;
    }
    else if (startNum === null && finishNum === null) {
        return 1;
    }
    else if (startNum < finishNum) {
        for (let index = 0; index <= number; index++) {
            arrayNumber.push(startNum);
            startNum = startNum + step;
        }
    }
    else if (startNum > finishNum) {
        for (let index = 0; index >= number; index--) {

            arrayNumber.push(startNum)
            startNum = startNum - step;
        }
    }
    return arrayNumber.reduce((a, b) => a + b)

}

console.log("\n");
console.log("Jawaban Soal 3");
console.log(sum(1, 10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15, 10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 


function dataHandling(input) {
    for (let i = 0; i < input.length; i++) {
        for (let j = 0; j < 4; j++) {
            switch (j) {
                case 0:
                    console.log("Nomor ID: " + input[i][j])
                    break;
                case 1:
                    console.log("Nama Lengkap: " + input[i][j])
                    break;
                case 2:
                    console.log("TTL: " + input[i][j] + " " + input[i][j + 1])
                    break;
                case 3:
                    console.log("Hobi: "+ input[i][j + 1])
                    break;
                default:
                    break;
            }

        }
        console.log("\n");
    }
}



var input2 = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]


console.log("\n");
console.log("Jawaban Soal 4");
dataHandling(input2)

function balikKata(str){
    let reversed = "";    
    for (var i = str.length - 1; i >= 0; i--){        
      reversed += str[i];
    }    
    return reversed;
  }

console.log("\n");
console.log("Jawaban Soal 5");
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 


var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
console.log("\n");
console.log("Jawaban Soal 6");
dataHandling2(input);
 

function dataHandling2(input){
    var Tanggal;
    var NamaBulan;
    input.splice(4,1);
    input.splice(4,0, "Pria", "SMA Internasional Metro");
    input.splice(1,1,input[1]+ "Elsharawy")
    input.splice(2,1,"Provinsi " +  input[2])
    console.log(input)
    switch (input[3].substring(3,5)) {
        case "01"  : NamaBulan = "Januari";   break;
        case "02"  : NamaBulan = "Februari";   break;
        case "03"  : NamaBulan = "Maret";   break;
        case "04"  : NamaBulan = "April";  break;
        case "05"  : NamaBulan = "Mei";  break;
        case "06"  : NamaBulan = "Juni";   break;
        case "07"  : NamaBulan = "Juli";  break;
        case "08"  : NamaBulan = "Agustus";  break;
        case "09"  : NamaBulan = "September";  break;
        case "10" : NamaBulan = "Oktober";  break;
        case "11" : NamaBulan = "November";  break;
        case "12" : NamaBulan = "Desember";  break;
    }
    console.log(NamaBulan)
    Tanggal = input[3].split('/');
    Tanggal.sort(function(a, b){return b-a});
    console.log(Tanggal)
    Tanggal = input[3].split('/');
    Tanggal = Tanggal.join('-')
    console.log(Tanggal)
    console.log(input[1].slice(0,14))
}