class Animal {
    constructor(name) {
        this.nameAnimal = name;
        this.legsAnimal = 4;
        this.cold_bloodedAnimal = false;
    }

    get name() {
        return this.nameAnimal;
    }

    get legs() {
        return this.legsAnimal;
    }

    get cold_blooded() {
        return this.cold_bloodedAnimal;
    }
}

class Frog extends Animal {
    constructor(name) {
        super(name);
    }

    jump() {
        console.log("hop hop");
    }
}

class Ape extends Animal {
    constructor(name) {
        super(name);
        this.legsAnimal = 2;
    }

    yell() {
        console.log("Auooo");
    }
}

class Clock {
    constructor({template}) {
        this.timer;
        this.template = template;
    }

    render() {
        var date = new Date();

        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;
        var output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);
       
        console.log(output);


    }

    stop(){
        clearInterval(timer);
    }

    start() {
        this.render();
       this.timer = setInterval(() => {
            this.render();
       }, 1000);
    }
}




var sheep = new Animal("shaun");

console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"

var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 

var clock = new Clock({ template: 'h:m:s' });
clock.start();  